CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Fetch some data about your Drupal website using a plugin system.

The collected data will be send to an API of your choice
using a JWT authorization.

REQUIREMENTS
------------

PHP library php-jwt.


INSTALLATION
------------

* composer config repositories.actency/health-monitor-client vcs git@gitlab.com:actency/utils/app-monitoring-publique/clients/health_monitor_client.git
* composer require actency/health_monitor_client
* vendor/bin/drush en -y health_monitor_client


CONFIGURATION
-------------

In your settings file, add the following lines :

```
$settings['hmc_jwt_secret'] = '[The JWT secret of your application]';
$settings['hmc_jwt_algorithm'] = 'HS256';
$settings['hmc_project'] = '[The project name]';
$settings['hmc_environment'] = '[The current environment]';
$settings['hmc_app_kind'] = 'drupal';
$settings['hmc_app_version'] = 9;
$settings['hmc_server_endpoint'] = '[The api endpoint]';
```


MAINTAINERS
-----------

 * Hakim Rachidi - https://www.drupal.org/u/hakimr
 * Leo Prada - https://www.drupal.org/u/nixou
 * Nicolas Loye - https://www.drupal.org/u/nicoloye
