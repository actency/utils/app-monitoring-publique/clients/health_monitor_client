<?php

namespace Drupal\health_monitor_client\Plugin;

use Drupal\health_monitor_client\Annotation\Checkpoint;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a Checkpoint plugin manager.
 *
 * @see \Drupal\health_monitor_client\Annotation\Checkpoint
 * @see \Drupal\health_monitor_client\CheckpointInterface
 * @see plugin_api
 */
class CheckpointManager extends DefaultPluginManager {

  /**
   * Constructs a new class instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Checkpoint', $namespaces, $module_handler, CheckpointInterface::class, Checkpoint::class);
    $this->alterInfo('checkpoint_info');
    $this->setCacheBackend($cache_backend, 'checkpoint_info', ['checkpoint_plugins']);
  }

}
