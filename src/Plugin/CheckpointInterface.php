<?php

namespace Drupal\health_monitor_client\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * An interface for checkpoint plugins.
 */
interface CheckpointInterface extends PluginInspectionInterface {

  /**
   * Retrieve information about the checkpoint.
   *
   * @return mixed
   *   Information extracted.
   */
  public function extract();

}
