<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Execute composer audit and return results.
 *
 * @Checkpoint(
 *   id = "composer_audit",
 *   label = @Translation("Composer Audit")
 * )
 */
class ComposerAudit extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
    public function extract() {
      $results = [];
      
      // Vérifier si la fonction exec existe
      if (!function_exists('exec')) {
          // Retourner un résultat indiquant que le test n'est pas applicable
          return $results;
      }

      // Exécuter la commande 'composer audit' et capturer la sortie
      $output = [];
      $return_var = 0;
      exec('composer audit --working-dir=' . DRUPAL_ROOT . '/../ --format=json', $output, $return_var);

      // Si la commande réussit, parser les résultats JSON
      if ($return_var === 1 && !empty($output)) {
          $audit_results = json_decode(implode('', $output), true);
          
          if (isset($audit_results['advisories'])) {
              foreach ($audit_results['advisories'] as $package => $issues) {
                  $results[$package] = $issues;
              }
          }
      }

      return $results;
  }
}
