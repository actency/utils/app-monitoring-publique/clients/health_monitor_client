<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * List dumps presents in the docroot.
 *
 * @Checkpoint(
 *   id = "dump_into_docroot",
 *   label = @Translation("Dumps into docroot")
 * )
 */
class DumpIntoDocroot extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Construct a Dump Into Docroot plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return array_keys($this->fileSystem->scanDirectory(DRUPAL_ROOT, '/.*\.(sql|gz|tar|dump|dmp|zip)$/', [
      'recurse' => TRUE,
    ]));
  }

}
