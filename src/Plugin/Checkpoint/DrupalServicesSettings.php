<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Retrieve Drupal Services settings.
 *
 * @Checkpoint(
 *   id = "drupal_services_settings",
 *   label = @Translation("Drupal Services Settings")
 * )
 */
class DrupalServicesSettings extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $results = [];

    $results = [
      'http_header_debug' => (int) \Drupal::getContainer()->getParameter('http.response.debug_cacheability_headers'),
      'twig' => [
        'auto_reload' => (int) \Drupal::getContainer()->getParameter('twig.config')['auto_reload'],
        'cache' => (int) \Drupal::getContainer()->getParameter('twig.config')['cache'],
        'debug' => (int) \Drupal::getContainer()->getParameter('twig.config')['debug'],
      ],
    ];

    return $results;
  }

}
