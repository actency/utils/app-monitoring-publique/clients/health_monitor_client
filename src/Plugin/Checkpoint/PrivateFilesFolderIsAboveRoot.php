<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Site\Settings;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Check if the the private files folder path is above Drupal root folder.
 *
 * @Checkpoint(
 *   id = "private_files_folder_is_above_root",
 *   label = @Translation("Private Files Folder Is Above Root")
 * )
 */
class PrivateFilesFolderIsAboveRoot extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $result = [];

    if ($files_private_path = Settings::get('file_private_path')) {
      $result = [
        'drupal_root_path' => realpath(DRUPAL_ROOT),
        'files_private_folder_path' => realpath($files_private_path),
      ];
    }

    return $result;
  }

}
