<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * List dumps presents in the docroot.
 *
 * @Checkpoint(
 *   id = "password_into_docroot",
 *   label = @Translation("Password into docroot")
 * )
 */
class PasswordIntoDocroot extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
    public function extract() {
      // Exécuter la commande grep et capturer la sortie
      $results = [];
      // Vérifier si la fonction exec existe
      if (!function_exists('exec')) {
          // Retourner un résultat vide si exec n'est pas disponible
          return $results;
      }

      exec('grep -rinE "(http|https):\/\/[^:@ \/]*:[^:@ \/]*@[^ ]*.(com|fr)?" --exclude=*.{sql,tar,zip,gz,jpg,png,jpeg,gz,mp4,svg,woff2,gif,JPG,mp3,webp,ttf} --exclude-dir=libraries --exclude-dir=node_modules --exclude-dir=files', $results);

      // Ne pas conserver les mots de passe trouvés dans le résultat pour éviter de les stocker
      foreach ($results as &$result) {
          $result = explode(':', $result);
          $file = $result[0];
          $line = $result[1];
          unset($result[0], $result[1]);

          $result = implode(':', $result);
          $result = $file . ':' . $line . ':' . explode('@', $result)[1];
      }

      return $results;
  }
}
