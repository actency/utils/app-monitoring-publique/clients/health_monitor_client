<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Core\Site\Settings;
use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Retrieve trusted hosts.
 *
 * @Checkpoint(
 *   id = "trusted_hosts",
 *   label = @Translation("Trusted Hosts")
 * )
 */
class TrustedHosts extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return Settings::get('trusted_host_patterns');
  }

}
