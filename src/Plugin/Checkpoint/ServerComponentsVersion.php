<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\system\SystemManager;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve server components version.
 *
 * @Checkpoint(
 *   id = "server_components_version",
 *   label = @Translation("Server Components Version")
 * )
 */
class ServerComponentsVersion extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The system manager service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * Construct a Server Components Version plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\system\SystemManager $system_manager
   *   The system manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SystemManager $system_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->systemManager = $system_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('system.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $requirements = $this->systemManager->listRequirements();

    return [
      'database_system' => $requirements['database_system']['value']->__toString(),
      'database_version' => $requirements['database_system_version']['value'],
      'php' => explode(' ', $requirements['php']['value']->__toString())[0],
    ];
  }

}
