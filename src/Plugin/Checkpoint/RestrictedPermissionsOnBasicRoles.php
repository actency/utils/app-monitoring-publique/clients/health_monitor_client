<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Drupal\user\PermissionHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve restricted permission list for anonymous and authenticated roles.
 *
 * @Checkpoint(
 *   id = "restricted_permissions_on_basic_roles",
 *   label = @Translation("Restricted Permissions On Basic Roles")
 * )
 */
class RestrictedPermissionsOnBasicRoles extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct a RestrictedPermissionsOnBasicRoles plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\user\PermissionHandlerInterface $permissionHandler
   *   The permission handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PermissionHandlerInterface $permissionHandler, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->permissionHandler = $permissionHandler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.permissions'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $all_permissions = $this->permissionHandler->getPermissions();

    // Use the entity type manager to load roles.
    $roles = ['anonymous', 'authenticated'];
    $role_storage = $this->entityTypeManager->getStorage('user_role');
    $role_entities = $role_storage->loadMultiple($roles);

    $permissions_per_role = [];
    foreach ($roles as $rid) {
      $permissions_per_role[$rid] = $role_entities[$rid] ? $role_entities[$rid]->getPermissions() : [];
    }

    // Sort out restricted permissions only.
    $restricted_permissions_per_role = [];
    foreach ($permissions_per_role as $role => $permissions) {
      foreach ($permissions as $permission) {
        if (isset($all_permissions[$permission]['restrict access'])) {
          $restricted_permissions_per_role[$role][$permission] = $permission;
        }
      }
    }

    return $restricted_permissions_per_role;
  }

}
