<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Core\Site\Settings;
use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Retrieve Drupal Cache Bins settings.
 *
 * @Checkpoint(
 *   id = "drupal_cache_bins_settings",
 *   label = @Translation("Drupal Cache Bins Settings")
 * )
 */
class DrupalCacheBinsSettings extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return Settings::get('cache');
  }

}
