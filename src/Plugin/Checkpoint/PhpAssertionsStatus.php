<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Retrieve php assertions status.
 *
 * @Checkpoint(
 *   id = "php_assertions_status",
 *   label = @Translation("Php Assertions Status")
 * )
 */
class PhpAssertionsStatus extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return assert_options(ASSERT_ACTIVE);
  }

}
