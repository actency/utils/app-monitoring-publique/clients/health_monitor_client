<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve password policies.
 *
 * @Checkpoint(
 *   id = "password_policies",
 *   label = @Translation("Password Policies")
 * )
 */
class PasswordPolicies extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct a Password Policies plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              ModuleHandlerInterface $module_handler,
                              EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $results = [];

    if ($this->moduleHandler->moduleExists('password_policy')) {
      foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
        $results[$role->id()] = [];

        $policies = $this->entityTypeManager->getStorage('password_policy')->loadByProperties(['roles.' . $role->id() => $role->id()]);
        /** @var \Drupal\password_policy\Entity\PasswordPolicy $policy */
        foreach ($policies as $policy) {
          foreach ($policy->getConstraints() as $constraint) {
            $results[$role->id()][$constraint['id']] = $constraint;
          }
        }
      }
    }

    return $results;
  }

}
