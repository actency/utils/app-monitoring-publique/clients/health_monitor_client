<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve the configurations on search api indexes.
 *
 * @Checkpoint(
 *   id = "search_api_indexes",
 *   label = @Translation("SearchApi Indexes")
 * )
 */
class SearchApiIndexes extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct an Available Modules plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              ModuleHandlerInterface $module_handler,
                              EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $results = [];

    if ($this->moduleHandler->moduleExists('search_api')) {
      /** @var \Drupal\search_api\Entity\Index $indexes */
      if ($indexes = $this->entityTypeManager->getStorage('search_api_index')->loadMultiple()) {
        /** @var \Drupal\search_api\Entity\Index $index */
        foreach ($indexes as $key => $index) {
          $server = $index->getServerId();
          $server_status = $index->isServerEnabled();
          $index_status = $index->status();
          $index_options = $index->getOptions();
          $index_processors = $index->getProcessors();
          $index_entity_types = $index->getEntityTypes();
          $index_fields = $index->getFields();

          $results[$key] = [
            'server' => [
              'server' => $server,
              'status' => $server_status,
            ],
            'index' => [
              'status' => $index_status,
              'options' => $index_options,
              'processors' => array_keys($index_processors),
              'entity_types' => array_keys($index_entity_types),
              'fields' => array_keys($index_fields),
            ],
          ];
        }
      }
    }

    return $results;
  }

}
