<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Core\Site\Settings;
use Drupal\Component\Plugin\PluginBase;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;

/**
 * Retrieve yaml services loaded.
 *
 * @Checkpoint(
 *   id = "yaml_services_loaded",
 *   label = @Translation("Yaml Services Loaded")
 * )
 */
class YamlServicesLoaded extends PluginBase implements CheckpointInterface {

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return Settings::get('container_yamls');
  }

}
