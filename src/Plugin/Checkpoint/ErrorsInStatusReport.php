<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\system\SystemManager;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve errors from status report.
 *
 * @Checkpoint(
 *   id = "errors_in_status_report",
 *   label = @Translation("Errors In Status Report")
 * )
 */
class ErrorsInStatusReport extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The system manager service.
   *
   * @var \Drupal\system\SystemManager
   */
  protected $systemManager;

  /**
   * The module installer service.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * Construct an Error In Status Report plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\system\SystemManager $system_manager
   *   The system manager service.
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $module_installer
   *   The module installer service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              SystemManager $system_manager,
                              ModuleInstallerInterface $module_installer,
                              ModuleHandlerInterface $module_handler,
                              KeyValueFactoryInterface $key_value_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->systemManager = $system_manager;
    $this->moduleInstaller = $module_installer;
    $this->moduleHandler = $module_handler;
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('system.manager'),
      $container->get('module_installer'),
      $container->get('module_handler'),
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $results = [];

    $updateModuleInstalled = $this->moduleHandler->moduleExists('update');

    if (!$updateModuleInstalled) {
      $this->moduleInstaller->install(['update']);
    }

    $this->keyValueFactory->get('update_fetch_task')->deleteAll();
    update_refresh();
    update_fetch_data();

    $requirements = $this->systemManager->listRequirements();
    foreach ($requirements as $requirement) {
      if (isset($requirement['severity'])
        && $requirement['severity'] == $this->systemManager::REQUIREMENT_ERROR
      ) {
        $results[] = [
          'title' => is_object($requirement['title']) ? $requirement['title']->__toString() : $requirement['title'],
          'value' => is_object($requirement['value']) ? $requirement['value']->__toString() : $requirement['value'],
        ];
      }
    }

    if (!$updateModuleInstalled) {
      $this->moduleInstaller->uninstall(['update']);
    }

    return $results;
  }

}
