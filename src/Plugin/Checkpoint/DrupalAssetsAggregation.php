<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve Drupal assets aggregation settings.
 *
 * @Checkpoint(
 *   id = "drupal_assets_aggregation",
 *   label = @Translation("Drupal Assets Aggregation")
 * )
 */
class DrupalAssetsAggregation extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct a Drupal Assets Aggregation plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    return [
      'css' => (int) $this->configFactory->get('system.performance')->get('css.preprocess'),
      'js' => (int) $this->configFactory->get('system.performance')->get('js.preprocess'),
    ];
  }

}
