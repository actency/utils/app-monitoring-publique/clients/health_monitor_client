<?php

namespace Drupal\health_monitor_client\Plugin\Checkpoint;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\health_monitor_client\Plugin\CheckpointInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * List docroot settings files containing sensitive data.
 *
 * @Checkpoint(
 *   id = "sensitive_docroot_settings_files",
 *   label = @Translation("Sensitive Docroot Settings Files")
 * )
 */
class SensitiveDocrootSettingsFiles extends PluginBase implements CheckpointInterface, ContainerFactoryPluginInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Construct a Sensitive Docroot Settings Files plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function extract() {
    $results = [];

    $phpFiles = array_keys($this->fileSystem->scanDirectory(DRUPAL_ROOT . '/sites', '/.*\.(php)$/', [
      'recurse' => TRUE,
    ]));

    foreach ($phpFiles as $phpFile) {
      $content = file_get_contents($phpFile);

      preg_match_all('/^\$databases[^;]*;/m', $content, $databaseSnippets);
      foreach ($databaseSnippets[0] as $databaseSnippet) {
        if (($databaseSnippet != '$databases = [];')
          && $databaseSnippet != '$databases = array();') {
          $results[] = $phpFile;
        }
      }

      preg_match_all('/^\$settings\[\'hash_salt\'\][^;]*;/m', $content, $hashSnippets);
      foreach ($hashSnippets[0] as $hashSnippet) {
        if ($hashSnippet != '$settings[\'hash_salt\'] = \'\';') {
          $results[] = $phpFile;
        }
      }
    }
    $results = array_unique($results);

    return $results;
  }

}
