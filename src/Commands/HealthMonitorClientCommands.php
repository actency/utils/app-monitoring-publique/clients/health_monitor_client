<?php

namespace Drupal\health_monitor_client\Commands;

use Drupal\health_monitor_client\Service\HealthMonitorClientManager;
use Drush\Commands\DrushCommands;

/**
 * Defines Drush commands for the Health Monitor Client module.
 */
class HealthMonitorClientCommands extends DrushCommands {

  /**
   * The health monitor client manager.
   *
   * @var \Drupal\health_monitor_client\Service\HealthMonitorClientManager
   */
  protected $healthMonitorClientManager;

  /**
   * Constructs a HealthMonitorClientCommands object.
   *
   * @param \Drupal\health_monitor_client\Service\HealthMonitorClientManager $healthMonitorClientManager
   *   The health monitor client manager.
   */
  public function __construct(HealthMonitorClientManager $healthMonitorClientManager) {
    parent::__construct();

    $this->healthMonitorClientManager = $healthMonitorClientManager;
  }

  /**
   * Run the checkpoint information extraction.
   *
   * @command health-monitor-client:run-checkpoint-information-extraction
   *
   * @usage drush health-monitor-client:run-checkpoint-information-extraction
   *   Run the checkpoint information extraction.
   *
   * @aliases hmc-rcie
   *
   * @throws \exception
   */
  public function runCheckpointInformationExtraction() {
    $this->healthMonitorClientManager->extract();
  }

  /**
   * Decode a JWT encoded string (for debug purpose only).
   *
   * @param string $jwt
   *   The JWT string.
   * @param string $secret
   *   The secret key.
   * @param string $algorithm
   *   The algorithm to use for decoding.
   *
   * @command health-monitor-client:decode-jwt
   *
   * @usage drush health-monitor-client:decode-jwt [JWT string] [secret key] [decoding algorithm]
   *   Decode a JWT encoded string (for debug purpose only).
   *
   * @aliases hmc-djwt
   *
   * @throws \Exception
   */
  public function decodeJwt(string $jwt, string $secret, string $algorithm) {
    $this->healthMonitorClientManager->decode($jwt, $secret, $algorithm);
  }

}
