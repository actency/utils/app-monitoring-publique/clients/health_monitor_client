<?php

namespace Drupal\health_monitor_client\Service;

use Drupal\Core\Url;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Drupal\Core\Site\Settings;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\health_monitor_client\Plugin\CheckpointManager;

/**
 * Manager for Health Monitor Client.
 */
class HealthMonitorClientManager {

  /**
   * The health monitor client checkpoint manager.
   *
   * @var \Drupal\health_monitor_client\Plugin\CheckpointManager
   */
  protected $checkpointManager;

  /**
   * HTTP client factory.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Construct a Health Monitor Client Manager instance.
   *
   * @param \Drupal\health_monitor_client\Plugin\CheckpointManager $checkpointManager
   *   The checkpoint manager.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(CheckpointManager $checkpointManager,
                              ClientInterface $http_client,
                              LoggerChannelFactoryInterface $logger_factory) {
    $this->checkpointManager = $checkpointManager;
    $this->httpClient = $http_client;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Extract checkpoint information and send them to an external endpoint.
   */
  public function extract() {
    if (($checkpoints = $this->loadPlugins())
      && ($data = $this->compile($checkpoints))
      && ($encodedData = $this->encode($data))
    ) {
      $this->send($encodedData);
    }

  }

  /**
   * Load checkpoint plugins.
   *
   * @return array
   *   An array of checkpoint plugins.
   */
  public function loadPlugins() {
    $checkpoints = [];

    $definitions = $this->checkpointManager->getDefinitions();
    foreach ($definitions as $definition) {
      try {
        $checkpoints[$definition['id']] = $this->checkpointManager->createInstance($definition['id']);
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('health_monitor_client')
          ->log('error', 'Error while loading the plugin @id : @message', [
            '@id' => $definition['id'],
            '@message' => $e->getMessage(),
          ]);
      }
    }

    return $checkpoints;
  }

  /**
   * Execute plugins and compile their data.
   *
   * @param array $plugins
   *   The list of plugins to execute.
   *
   * @return array
   *   An array of information provided by checkpoint plugins.
   */
  public function compile(array $plugins) {
    $data = [];

    foreach ($plugins as $id => $plugin) {
      try {
        $data[$id] = $plugin->extract();
      }
      catch (\Exception $e) {
        $this->loggerFactory->get('health_monitor_client')
          ->log('error', 'Error while compiling the plugin @id : @message', [
            '@id' => $id,
            '@message' => $e->getMessage(),
          ]);
      }
    }

    ksort($data);

    return $data;
  }

  /**
   * Encode data using JWT.
   *
   * @param array $payload
   *   The data to encode.
   *
   * @return array
   *   The header and body encoded.
   *
   * @throws \exception
   */
  public function encode(array $payload): array {
    $header = [
      'sub' => Settings::get('hmc_project'),
      'exp' => time() + 120,
    ];

    $payload['sub'] = Settings::get('hmc_project');
    $payload['metadata'] = [
      'project' => Settings::get('hmc_project'),
      'environment' => Settings::get('hmc_environment'),
      'app_kind' => Settings::get('hmc_app_kind'),
      'app_version' => Settings::get('hmc_app_version'),
    ];

    if (!($secret = Settings::get('hmc_jwt_secret'))) {
      $this->loggerFactory->get('health_monitor_client')
        ->log('error', 'JWT secret is missing');
      throw new \Exception('Unable to encode the output. JWT secret could not be retrieved.');
    }

    // If a specific algorithm is defined we use it,
    // otherwise we use the default one.
    if ($algorithm = Settings::get('hmc_jwt_algorithm')) {
      return [
        'header' => JWT::encode($header, $secret, $algorithm),
        'body' => json_encode($payload),
      ];
    }
    else {
      return [
        'header' => JWT::encode($header, $secret),
        'body' => json_encode($payload),
      ];
    }
  }

  /**
   * Decode data using JWT (debug purpose only).
   *
   * @param string $jwt
   *   The JWT string.
   * @param string $secret
   *   The secret key.
   * @param string $algorithm
   *   The algorithm to use for decoding.
   *
   * @throws \Exception
   */
  public function decode(string $jwt, string $secret, string $algorithm) {
    var_dump(JWT::decode($jwt, new Key($secret, $algorithm)));
  }

  /**
   * Send encoded data to an external endpoint.
   *
   * @param array $request
   *   The body and header of the request.
   */
  public function send(array $request) {
    $url = Url::fromUri(Settings::get('hmc_server_endpoint') . '/' . Settings::get('hmc_project'));

    try {
      $this->httpClient->request('POST', $url->toString(), [
        'verify' => FALSE,
        RequestOptions::HEADERS => [
          'Authorization' => 'Bearer ' . $request['header'],
          'Content-Type' => 'text/plain',
          'Content-Length' => strlen($request['body']),
        ],
        RequestOptions::BODY => $request['body'],
      ]);
    }
    catch (\Exception $e) {
      $this->loggerFactory->get('health_monitor_client')
        ->log('error', 'Error while sending data to the endpoint : @message', [
          '@message' => $e->getMessage(),
        ]);
    }
  }

}
