<?php

namespace Drupal\health_monitor_client\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Health Monitor Client checkpoint annotation object.
 *
 * Plugin Namespace: Plugin\Checkpoint.
 *
 * @see \Drupal\health_monitor_client\Plugin\CheckpointInterface
 * @see \Drupal\health_monitor_client\Plugin\CheckpointManager
 * @see checkpoint_info_alter()
 * @see plugin_api
 *
 * @Annotation
 */
class Checkpoint extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the action.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

}
